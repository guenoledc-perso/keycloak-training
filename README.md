# keycloak training

to clone the projet, go to the receiving folder and
```sh
git clone https://gitlab.com/guenoledc-perso/keycloak-training.git
cd keycloak-training
```

## Launch docker environment
```sh
cd docker
docker-compose up -d

docker-compose logs -f
```

## Connect 

* to keycloak admin : http://localhost:10000/auth. Default admin user : admin/admin.
* to mail dev : http://localhost:10000/maildev
* to oidc tester : http://localhost:10000/oidc-tester


## Creating the first Realm and OIDC Client

1. Go to keycloak admin and connect
1. By default you are on master realm. 
1. Create a new realm : training
1. In the realm settings : set Frontend URL to http://localhost:10000/auth; then Save
1. Go to Login tab; activate User registration; then Save
1. In the realm, go to Clients
1. Create new client ID: lesson1; protocol: openid-connect
1. In settings, set Access Type to confidential (meaning that the client requires a secret to communicate with the realm)
1. set Valid Redirect URIs: http://localhost:10000/oidc-tester/session/callback
1. Save the settings, all is ready

## Testing the authentication

1. Go to oidc tester and select training config (a preset config)
1. Edit the configuration and locate the Client secret field (bottom of the page)
1. From keycloak realm clients, go to `lesson1` client and go to Credentials page
1. Copy the secret in the clipboard 
1. Paste the secret to the oidc tester config and save
1. Click login, and follow the registration process in keycloak
1. You should have the structure of the authentication

## Requesting password reset

1. Activate password reset function in the realm
1. Define the email server for the realm : maildev / 25
1. Ensure you are logout from oidc tester and try login
1. In the login form, click the forgot password and follow the process
1. Get the email in the maildev screen

