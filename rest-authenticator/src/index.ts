// Disabling camelcase because oidc standard fields are not camelcase
/* eslint-disable @typescript-eslint/camelcase */
import dotenv from "dotenv";
dotenv.config();

import express from "express";

import BodyParser from "body-parser";
import logger from "./logger";

import KeycloakRestAuthenticator from "keycloak-rest-authenticator";

import {SimpleAuthenticator} from "./usecase/simple-authenticator";
import {WAYFAuthenticator} from "./usecase/whereareyoufrom-auth";

const app = express();
app.use(BodyParser.json());

const packager = new KeycloakRestAuthenticator.KeycloakScriptPackager({
  keycloakAccessibleBaseUrl: process.env.EXTERNAL_BASE_URL!,
  keycloakDeploymentLocation: process.env.KEYCLOAK_DEPLOYMENTS_PATH
});

app.use(KeycloakRestAuthenticator.RestAuthenticator.declare("/simple", new SimpleAuthenticator(), packager));
app.use(KeycloakRestAuthenticator.RestAuthenticator.declare("/wayf", new WAYFAuthenticator(), packager));


const port: number = Number.parseInt(process.env.PORT || "10088");
if (process.env.DO_NOT_LISTEN) {
  logger.info("DO NOT LISTEN env variable activated.");
} else {
  app.listen(port, () => {
    logger.info(`Authenticator microservice listening`, { port: port });
    packager.make();
  });
  process.on("SIGABRT", cleanTerminate);
  process.on("SIGINT", cleanTerminate);
  process.on("SIGBREAK", cleanTerminate);
}

export function closeAll(): void {
  
}

function cleanTerminate(signal: NodeJS.Signals): void {
  logger.info("cleaning before terminating process ...", { signal: signal });
  closeAll();
  process.exit(0);
}

export default app;
