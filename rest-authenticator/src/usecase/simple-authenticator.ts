import {KeycloakRestAuthenticator, AuthenticationRequest, AuthenticationResponse} from "keycloak-rest-authenticator"
import logger from "../logger";

export class SimpleAuthenticator implements KeycloakRestAuthenticator {
  constructor() {
  }

  async processNew(req: AuthenticationRequest): Promise<AuthenticationResponse | null> {
    logger.debug("SimpleAuthenticator (new) received", req.userSession)
    logger.debug("SimpleAuthenticator (new) received", {mode: req.mode, session:req.authSession})
    if(!req.user) {
      return {
        failure: "unknownUser"
      }
    }
    if(req.user?.attributes.PASSED_SECRET) {
      return {
        userSessionNotes: {
          hasPassedSecret: "automatically"
        }
      }
    }
    return {
      challenge: {
        formName: "login-password.ftl",
        message:"Enter the secret please"
      }
    }
  }
  async processInteraction(req: AuthenticationRequest): Promise<AuthenticationResponse | null> {
    logger.debug("SimpleAuthenticator (interaction) received", {mode: req.mode, session:req.authSession})
    logger.debug("SimpleAuthenticator (interaction) received", req.httpRequest.formParams)
    // SimpleAuthenticator (interaction) received { password: [ 'BLA' ] }
    const formParams = req.httpRequest.formParams;
    if(formParams.password && formParams.password.length==1) {
      if(formParams.password[0]=="BLA") {
        return {
          userAttributes: {
            PASSED_SECRET: "well done"
          },
          userSessionNotes: {
            hasPassedSecret: "form"
          }
        }
      }
    } 
    return {
      failure: "invalidCredentials",
      challenge: {
        formName: "login-password.ftl",
        message:"Try again to enter the secret"
      }
    }
    
  }

  async processInterruption(req: AuthenticationRequest): Promise<AuthenticationResponse | null> {
    throw new Error("Method not implemented.");
  }
}