import {KeycloakRestAuthenticator, AuthenticationRequest, AuthenticationResponse, AuthNoteModel} from "keycloak-rest-authenticator"
import logger from "../logger";

export class WAYFAuthenticator implements KeycloakRestAuthenticator {
  constructor() {
  }

  async processNew(req: AuthenticationRequest): Promise<AuthenticationResponse | null> {
    logger.debug("TestAuthenticator (interaction) mode", req.mode)
    logger.debug("TestAuthenticator (interaction) received", req.httpRequest.uri)
    const receivedUrl = new URL(req.httpRequest.uri)
    if(receivedUrl.searchParams.has("kc_idp_hint")) {
      // already defined, bypass and consider validated
      return {}
    } else {
      return {
        authNote: {url: receivedUrl.href},
        challenge: {
          formName: "login-username.ftl",
          message:"Enter your email to determine the idp",
          formData: {
           login: {
             username: req.authSession.clientNotes.login_hint
           }
          }
        }
      }
    }
  }
  async processInteraction(req: AuthenticationRequest): Promise<AuthenticationResponse | null> {
    logger.debug("TestAuthenticator (interaction) mode", req.mode)
    logger.debug("TestAuthenticator (interaction) received", req.httpRequest.formParams)
    const initialUrl = new URL(req.authSession.authNote.url);
    const email = req.httpRequest.formParams.username[0];
    const match = /.*@([a-zA-Z0-9_-]+)\.[a-zA-Z]+/.exec(email)
    if(!match) {
      return {
        challenge: {
          formName: "login-username.ftl",
          message:"Enter your email to determine the idp",
          formData: {
           login: {
           }
          }
        }
      }
    } else {
      logger.debug("email matcher", match)
      initialUrl.searchParams.append("kc_idp_hint", match[1])
      return {
        challenge: {
          redirectUri: initialUrl.href
        }
      }
    }

    
    
  }

  async processInterruption(req: AuthenticationRequest): Promise<AuthenticationResponse | null> {
    throw new Error("Method not implemented.");
  }
}